fn even_or_odd(number: i32) -> &'static str {
    return if number % 2 == 0 {"Even"} else {"Odd"}
}