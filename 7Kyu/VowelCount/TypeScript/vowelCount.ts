export class Kata {
  static getCount(str: string) {
    return [...str].filter(char => 'aeiou'.includes(char)).length;
  }
}