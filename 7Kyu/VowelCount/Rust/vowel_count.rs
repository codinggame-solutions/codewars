fn get_count(s: &str) -> usize {
  s.matches(&['a', 'e', 'i', 'o', 'u'][..]).count()
}
